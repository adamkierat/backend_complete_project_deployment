from flask import Flask
from flask import render_template
from flask import jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/end1')
def end1():
   return jsonify({'text': "text"})

@app.route('/end2')
def end2():
   return {'text': "text"}


if __name__ == '__main__':
    app.run(debug=True,host = "0.0.0.0")
